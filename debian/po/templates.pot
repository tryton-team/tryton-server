# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the tryton-server package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: tryton-server\n"
"Report-Msgid-Bugs-To: tryton-server@packages.debian.org\n"
"POT-Creation-Date: 2022-09-27 21:21+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../tryton-server-postgresql.templates:1001
msgid "Initial admin password for Tryton:"
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-postgresql.templates:1001
msgid ""
"A superuser account named \"admin\" will be created for the Tryton database. "
"Please specify the password that this account should require for the initial "
"login."
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-postgresql.templates:1001
msgid ""
"If it is left empty a random password will be used. You can reset this "
"password from the command line with"
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-postgresql.templates:1001
msgid ""
"$ sudo -u tryton trytond-admin -c /etc/tryton/trytond.conf --password -d "
"<your_database_name>"
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-postgresql.templates:1001
msgid ""
"Note: The initialization of the database may take some time; please be "
"patient."
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-postgresql.templates:2001
msgid "Email address for the admin user:"
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-postgresql.templates:2001
msgid ""
"Please specify a valid email address that should receive administrative "
"messages from the Tryton server."
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-uwsgi.templates:1001
msgid "Set up Tryton server workers?"
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-uwsgi.templates:1001
msgid ""
"Some Tryton server tasks can be performed asynchronously in the background "
"by workers in a task queue."
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-uwsgi.templates:1001
msgid ""
"If enabled, the workers can automatically perform tasks such as the "
"processing of sales, invoices, or purchases removing the need to execute "
"particular workflow steps manually in the clients and wait for them to "
"finish."
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-uwsgi.templates:2001
msgid "Set up a Tryton scheduler?"
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-uwsgi.templates:2001
msgid ""
"Tryton server has its own internal \"cron\" scheduler which can run periodic "
"database-maintenance tasks (configured using the Tryton clients). Please "
"specify whether it should be enabled."
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-uwsgi.templates:2001
msgid "Note: Only one cron server should be enabled per database."
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-nginx.templates:1001
msgid "Domain for the Tryton website:"
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-nginx.templates:1001
msgid ""
"This will be the domain under which the Tryton server will be exposed via "
"http (and/or https with Letsencrypt certificates when using the python3-"
"certbot-nginx package)."
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-nginx.templates:1001
msgid ""
"The value should be a FQDN (Full Qualified Domain Name). Typically it is "
"something like \"mycompany.com\" or \"tryton.mysite.eu\"."
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-nginx.templates:1001
msgid ""
"If you leave the value empty the hostname of this machine will be used as "
"domain."
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-nginx.templates:2001
msgid "Perform Letsencrypt registration and configuration?"
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-nginx.templates:2001
msgid ""
"When the python3-certbot-nginx package is installed (default configuration) "
"the automatic registration with Letsencrypt can be performed. The domain as "
"configured in the previous question will be registered."
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-nginx.templates:2001
msgid ""
"Note: For this to work you need \n"
" - a fully registered domain name\n"
" - working DNS records for this domain name\n"
" - accessibility to this server via Internet on port 80/443"
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-nginx.templates:2001
msgid ""
"In case you want to register several (sub-)domains with this nginx frontend "
"you should opt out and perform the registration manually. You can perform "
"this configuration manually at any time later (please refer to the necessary "
"steps in /usr/share/doc/certbot/README.rst.gz)."
msgstr ""

#. Type: boolean
#. Description
#: ../tryton-server-nginx.templates:2001
msgid ""
"When registering with Letsencrypt you agree with the Terms of Service at "
"https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must "
"agree in order to register with the ACME server."
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-nginx.templates:3001
msgid "Letsencrypt E-Mail:"
msgstr ""

#. Type: string
#. Description
#: ../tryton-server-nginx.templates:3001
msgid "Enter an email address (used for urgent renewal and security notices)"
msgstr ""

#. Type: multiselect
#. Description
#: ../tryton-server-all-in-one.templates:1001
msgid "Postal codes"
msgstr ""

#. Type: multiselect
#. Description
#: ../tryton-server-all-in-one.templates:1001
msgid ""
"Please select all countries for which postal codes should be imported. This "
"allows the address management system to offer automatic completion features."
msgstr ""

#. Type: multiselect
#. Description
#: ../tryton-server-all-in-one.templates:1001
msgid ""
"Note: The availability of codes depends on their existence on geonames.org."
msgstr ""
