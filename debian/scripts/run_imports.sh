#!/bin/sh

_CODES="$@"
db_name="tryton"

# get database settings from dbconfig-common
if [ -f /etc/dbconfig-common/tryton-server-postgresql.conf ]; then
    . /etc/dbconfig-common/tryton-server-postgresql.conf
fi

if [ ! -z "$dbc_dbname" ]; then
    db_name=$dbc_dbname
fi

/usr/bin/trytond_import_currencies -c /etc/tryton/trytond.conf -d "$db_name" || true
/usr/bin/trytond_import_countries -c /etc/tryton/trytond.conf -d "$db_name" || true
if [ ! -z "$_CODES" ]; then
    /usr/bin/trytond_import_postal_codes -c /etc/tryton/trytond.conf -d "$db_name" $_CODES || true
fi
